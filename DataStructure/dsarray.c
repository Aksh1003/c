// Menu driven program for arrays
#include<stdio.h>
void create(int a[],int n)
{
	int i;
	printf("\n enter array elements");
	for(i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
}
void display(int a[],int n)
{
	int i;
	printf("\n array is\n");
	for(i=0;i<n;i++)
	{
		printf("\n%d\n",a[i]);
	}
}
void insert(int a[],int pos,int key,int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		a[i]=a[i-1];
	}
	a[pos-1]=key;
}
void delete(int a[],int pos,int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		a[i]=a[i+1];
	}
	a[i-1]=0;
}
void search(int a[],int key,int n)
{
	int temp=0,i;
	for(i=0;i<n;i++)
	{
		if(a[i]==key)
		{
			temp=1;
			break;
		}
	}
		if(temp==1)
		{
			printf("\n %d is present in the array",key);
		}
		else
		{
			printf("\n %d is present in the array",key);
		}
}
void main()
{
	int ch,array[100],pos,key,n;
	do
	{
		printf("\nMENU\n");
		printf("\n 1.create");
		printf("\n 2.display");
		printf("\n 3.insert");
		printf("\n 4.delete");
		printf("\n 5.search");
		printf("\n enter your choice : ");
		scanf("%d",&ch);
		switch(ch)
		{
			case 1:
				printf("\n enter how many elements : ");
				scanf("%d",&n);
				create(array,n);
				break;
			case 2:
				display(array,n);
				break;
			case 3:
				printf("\n enter position");
				scanf("%d",&pos);
				if(pos<0 || pos>=n)
				{
					printf("\n invalid position");
					break;
				}
				printf("\n enter number");
				scanf("%d",&key);
				insert(array,pos,key,n);
				break;
			case 4:
				printf("\n enter position");
				scanf("%d",&pos);
				if(pos<0 || pos>=n)
				{
					printf("\n invalid position");
					break;
				}
				delete(array,pos,n);
				break;
			case 5:
				printf("\n enter number");
				scanf("%d",&n);
				search(array,key,n);
				break;
			default:
				printf("\n pls enter valid choice.........");
		}
	}
	while(ch!=6);
}
