// Addition of sparse matrix
#include<stdio.h>
struct sparse
{
	int row,col,value;
};
void getsm(struct sparse a[])
{
	int i;
	printf("\n Enter number of rows and columns : ");
	scanf("%d %d"&a[0].row,&a[0].col);
	printf("\n enter number of non zero elements : ");
	scanf("%d",&a[0].value);
	for(i=1;i<=a[0].value;i++)
	{
		printf("\n enter the row : ");
		scanf("%d",&a[i].row);
		printf("\n enter the column : ");
		scanf("%d",&a[i].col);
		printf("\n enter the element : ");
		scanf("%d",&a[i].value);
	}
}
void putsm(struct sparse a[])
{
	int i;
	printf("\n row\tcolumn\tvalue\n");
	for(i=1;i<=a[0].value;i++)
	{
		printf("\n%d\t%d\t%d\n",a[i].row,a[i].col,a[i].value);
	}
}
void addsm(struct sparse a[],struct sparse b[],struct sparse c[])
{
	int i,j,k,m=a[0].value,n=b[0].value,count=0;
	i=j=k=1;
	c[0].row=a[0].row;
	c[0].col=a[0].col;
	while(i<=m && j<=n)
	{
		count++;
		if(a[i].row==b[j].row)
		if(a[i].col==b[j].col)
		{
			c[k].row=a[i].row;
			c[k].col=a[i].col;
			c[k].value=a[i].value+b[j].value;
			i++;
			j++;
			k++;
		}
		else if(a[i].col<b[j].col)
		{
			c[k++]=a[i++];
		}
		else
		{
			c[k++]=b[j++];
		}
		else if(a[i].row<b[j].row)
		{
			c[k++]=a[i++];
		}
		else
		{
			c[k++]=b[j++];
		}
	}
	while(i<=m)
	{
		count++;
		c[k++]=a[i++];
	}
	while(j<=n)
	{
		count++;
		c[k++]=b[j++];
	}
	c[0].value=count;
}
void main()
{
	struct sparse a[10],b[10],c[10];
	printf("\n enter sparse matrix 1 : ");
	getsm(a);
	printf("\n enter sparse matrix 2 : ");
	getsm(b);
	addsm(a,b,c);
	printf("\n the first sparse matrix is : \n");
	putsm(a);
	printf("\n the second sparse matrix is : \n");
	putsm(b);
	printf("\n the addition of sparse matrix is : \n");
	putsm(c);
}
