// Implementation of bubble sort
#include<stdio.h>
void bubble(int a[],int n)
{
	int i,j,t;
	for(i=n-2;i>=0;i--)
	{
		for(j=0;j<=i;j++)
		{
			if(a[j]>a[j+1])
			{
				t=a[j];
				a[j]=a[j+1];
				a[j+1]=t;
			}
		}
	}
}
main()
{
	int a[100],n,i;
	printf("enter integer value for total no of elements to be sorted\n");
	scanf("%d",&n);
	for(i=0;i<=n-1;i++)
	{
		printf("enter integer value for element no %d : ",i+1);
		scanf("%d",&a[i]);
	}
	bubble(a,n);
	printf("finally sorted array is : \n");
	for(i=0;i<=n-1;i++)
	printf("%3d",a[i]);
}

