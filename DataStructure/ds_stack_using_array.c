//stack using array
#include<stdio.h>
#define max 50
void push();
void pop();
void display();
int menu();
int stack[max],top=0;
void main()
{
	int ch;
	do
	{
		ch=menu();
		switch(ch)
		{
			case 1: push();
				break;
			case 2: pop();
				break;
			case 3: display();
				break;
			default : printf("\n enter a valid choice!!!");
		}
	}
	while(1);
}
int menu()
{
	int ch;
	printf("\n stack");
	printf("\n1.push\n2.pop\n3.display\n4.exit");
	printf("enter choice : ");
	scanf("%d",&ch);
	return ch;
}
void push()
{
	if(top==max)
	printf("\n overflow");
	else
	{
		int ele;
		printf("\nenter element");
		scanf("%d",&ele);
		printf("\n element (%d) has been pushed at %d",ele,top);
		stack[top++]=ele;
	}
}
void pop()
{
	if(top==-1)
	printf("\n underflow");
	else
	{
		top--;
		printf("\n element has been poped out");
	}
}
void display()
{
	if(top==0)
	printf("\n stack is empty");
	else
	{
		int i;
		for(i=0;i<max;i++)
		printf("%d",stack[i]);
	}
}
