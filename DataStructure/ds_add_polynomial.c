// Addition of two polynomials using array
#include<stdio.h>
#define MAX 20
struct polynomial
{
	int exp;
	int coef;
};
void readpoly(struct polynomial p[],int n)
{
	int i;
	printf("\n enter the terms in descending order of power : \n");
	for(i=0;i<n;i++)
	{
		printf("\n enter coefficient and power of term : \n");
		scanf("%d%d",&p[i].coef,&p[i].exp);
	}
}
void disppoly(struct polynomial p[],int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		printf("%dx^%d ",p[i].coef,p[i].exp);
	}
}
int addpoly(struct polynomial x[],int m,struct polynomial y[],int n,struct polynomial z[])
{
	int i=0,j=0,k=0;
	while((i<m)&&(j<n))
	{
		if(x[i].exp==y[j].exp)
		{
			z[k].coef=x[i].coef+y[j].coef;
			z[k].exp=x[i].exp;
			i++;
			j++;
			k++;
		}
		else if(x[i].exp>y[j].exp)
		{
			z[k].coef=x[i].coef;
			z[k].exp=x[i].exp;
			i++;
			k++;
		}
		else
		{
			z[k].coef=y[j].coef;
			z[k].exp=y[j].exp;
			k++;
			j++;
		}
	}
	while(i<m)
	{
		z[k].coef=x[i].coef;
		z[k].exp=x[i].exp;
		i++;
		k++;
	}
	while(j<n)
	{
		z[k].coef=y[j].coef;
		z[k].exp=y[j].exp;
		j++;
		k++;
	}
	return(k);
}
void main()
{
	struct polynomial a[MAX],b[MAX],c[MAX];
	int m,n,x;
	printf("\n enter term of first polynomial : ");
	scanf("%d",&m);
	readpoly(a,m);
	printf("\n first polynomial is : \n");
	disppoly(a,m);
	printf("\n enter term of second polynomial : ");
	scanf("%d",&n);
	readpoly(b,n);
	printf("\n second polynomial is : \n");
	disppoly(b,n);
	printf("\n addition of two polynomial is : \n");
	x=addpoly(a,m,b,n,c);
	disppoly(c,x);
}
