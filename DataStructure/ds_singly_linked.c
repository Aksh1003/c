// Menu driven program for singly linked list
#include<stdio.h>
#define nodealloc (struct node*)malloc(sizeof(struct node))
struct node
{
	int data;
	struct node*next;
};
typedef struct node*nodeptr;
nodeptr create(struct node*head)
{
	int i,n;
	nodeptr p,current;
	printf("\n enter number of nodes ");
	scanf("%d",&n);
	for(i=1;i<=n;i++)
	{
		p=nodealloc;
		p->next=NULL;
		printf("\n enter the data ");
		scanf("%d",&p->data);
		if(head==NULL)
		{
			head=current=p;
		}
		else
		{
			current->next=p;
			current=p;
		}
	}
	return head;
}
nodeptr insert(nodeptr head,int pos,int num)
{
	int i;
	nodeptr p,current;
	p=nodealloc;
	p->data=num;
	if(pos==1)
	{
		p->next=head;
		head=p;
	}
	else
	{
		for(i=1,current=head;i<(pos-1)&&current->next!=NULL;i++,current=current->next);
		p->next=current->next;
		current->next=p;
	}
	return head;
}
void display(nodeptr head)
{
	nodeptr current=head;
	while(current!=NULL)
	{
		printf("%d\t",current->data);
		current=current->next;
	}
}
nodeptr delete(nodeptr head,int pos)
{
	int i;
	nodeptr p,current;
	if(pos==1)
	{
		p=head;
		head=head->next;
		free(p);
	}
	else
	{
		for(i=1,current=head;i<(pos-1)&&current->next!=NULL;i++,current=current->next);
		p=current->next;
		current->next=p->next;
		free(p);
	}
	return head;
}
void main()
{
	int num,pos,choice;
	nodeptr head=NULL;
	do
	{
		printf("\n1:create");
		printf("\n2:insert");
		printf("\n3:delete");
		printf("\n4:display");
		printf("\n5:exit");
		printf("\n enter your choice : ");
		scanf("%d",&choice);
		switch(choice)
		{
			case 1: head=create(head);
				break;
			case 2: printf("\n enter the element to insert : ");
				scanf("%d",&num);
				printf("\n enter the position : ");
				scanf("%d",&pos);
				head=insert(head,pos,num);
				break;
			case 3: printf("\n enter the position ");
				scanf("%d",&pos);
				head=delete(head,pos);
				break;
			case 4: printf("\n\n the linked list is : \n\n");
				display(head);
				printf("\n\n");
				break;
			default : printf("\n please enter valid choice.........");
		}
	}
	while(choice!=5);
}
