#include <stdio.h>

void find(int* ptr, int size)
{
	int i = 0, min = (int)&i, secmin = (int)&i, thimin = (int)&i;

	for (i = 0; i < size; i++)
	{
		if (ptr[i] < min)
		{
			thimin = secmin;
			secmin = min;
			min = ptr[i];
		}
		else if ((secmin >= ptr[i]) && (secmin >= min))
		{
			thimin = secmin;
			secmin = ptr[i];
		}
		else if ((thimin > ptr[i]) && (thimin > secmin))
		{
			thimin = ptr[i];
		}
	}

	printf("\nmin = %d\n",min);
	printf("\nsecmin = %d\n", secmin);
	printf("\nthimin = %d\n", thimin);

}

int main()
{
	int arr[] = {1, 50, 40, 30, 3, 20, 10, 2};
	int size = sizeof(arr);

	find(arr, size/sizeof(int));

	return 0;
}