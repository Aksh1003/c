// Swapping number by call by value,call by reference,call by address
#include<iostream>
using namespace std;
void swap1(int,int);
void swap2(int*,int*);
void swap3(int&x,int&y);
int main()
{
	int a,b,p,q;
	cout<<"enter value of a and b\n";
	cin>>a>>b;
	cout<<"enter value of p and q\n";
	cin>>p>>q;
	cout<<"\n values before swapping\n";
	swap1(a,b);
	swap2(&a,&b);
	cout<<"\n values after swapping\n"<<a<<b;	
	swap3(p,q);
	cout<<"\n values after swapping\n"<<p<<q;
	return 0;
}
void swap1(int a,int b)
{
	int t;
	t=a;
	a=b;
	b=t;
	cout<<"after swaping\n"<<"a="<<a<<"b="<<b;
}
void swap2(int *p,int *q)
{
	int t;
	t=*p;
	*p=*q;
	*q=t;
}
void swap3(int &x,int &y)
{
	int t;
	t=x;
	x=y;
	y=t;
}
