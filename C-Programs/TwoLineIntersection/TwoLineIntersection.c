#include <stdio.h>

#define AAB_MAIN main

struct AAB_AXIS
{
	double aab_x;
	double aab_y;
};

struct AAB_AXIS aab_A{1, 1}, aab_B{4, 4}, aab_C{1, 8}, aab_D{2, 4};

int AAB_MAIN()
{
	struct AAB_AXIS aab_result;

	// Declaration
	struct AAB_AXIS AAB_Intersection(struct AAB_AXIS, struct AAB_AXIS, struct AAB_AXIS, struct AAB_AXIS);

	// Call
	aab_result = AAB_Intersection(aab_A, aab_B, aab_C, aab_D);

	if(aab_result.aab_x == 0 && aab_result.aab_y)
		printf("\nLines Are Parallel\n");
	else
		printf("\n(X, Y) = (%d, %d)\n", aab_result.aab_x, aab_result.aab_y);

	return 0;
}

AAB_AXIS AAB_Intersection(struct AAB_AXIS aab_A, struct AAB_AXIS aab_B, struct AAB_AXIS aab_C, struct AAB_AXIS aab_D)
{
	// a1x + b1y = c1
	double aab_a1 = aab_B.aab_y - aab_A.aab_y;
	double aab_b1 = aab_A.aab_x - aab_B.aab_x;
	double aab_c1 = aab_a1 * (aab_A.aab_x) + aab_b1 * (aab_A.aab_y);

	// a2x + b2y = c2
	double aab_a2 = aab_D.aab_y - aab_C.aab_y;
	double aab_b2 = aab_C.aab_x - aab_D.aab_x;
	double aab_c2 = aab_a2 * (aab_C.aab_x) + aab_b2 * (aab_C.aab_y);

	double aab_determinant = (aab_a1 * aab_b2) - (aab_a2 - aab_b1);

	if (aab_determinant == 0)
		return {0, 0};
	else
	{
		double aab_x = (aab_a1 * aab_c2) - (aab_a2 * aab_c1);
		double aab_y = (aab_b1 * aab_c2) - (aab_b2 * aab_c1);

		return {aab_x, aab_y};
	}
}