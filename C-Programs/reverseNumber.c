#include <stdio.h>

int main()
{
	int no = 4321;
	int rev = 0;

	while (no != 0)
	{
		int digit = no % 10;

		rev = rev * 10 + digit;

		no /= 10;
	}

	printf("%d", rev);

	return 0;
}