// Size of pointer variable
#include<stdio.h>
main()
{
	char *cptr;
	int *iptr;
	float *fptr;
	printf("POinter to character takes %d bytes\n",sizeof(cptr));
	printf("POinter to integer takes %d bytes\n",sizeof(iptr));
	printf("POinter to float takes %d bytes\n",sizeof(fptr));
}
