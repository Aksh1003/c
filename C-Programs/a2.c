// Pointer to array
#include<stdio.h>
void simulatepointertoarray()
{
	int array[10]={10,20,30,40,50,60,70,80,90,100};
	int *ptr=array;
	int (*ptrarray)[10]=&array;
	int i=0;
	printf("%p\n %p\n %p\n",array,ptr,ptrarray);
	for(i=0 ; i<10 ; i++)
	{
		printf("Array value=%d\n",array[i]);
		printf("Array value using pointer=%d\n",ptr[i]);
		printf("Array value using pointer to an address=%p value=%d\n",ptrarray[i],(*(*ptrarray)+i));
		printf("Array value using pointer to an array address=%p value=%d\n",ptrarray[i],(*ptrarray)[i]);		
	}
}
int main()
{
	simulatepointertoarray();
	return 0;
}
