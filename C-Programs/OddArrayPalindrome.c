#include <stdio.h>

typedef char Bool;

#define true 1
#define false 0

Bool Palindrome(int* arr, int size)
{
	int i = 0, j = size - 1;

	while (i < j)
	{
		if (arr[i] != arr[j])
			break;

		i++;
		j--;
	}

	if (i == j)
		return true;
	else
		return false;
}

int main()
{
	int arr[] = {10, 20, 30, 20, 10};
	int size = sizeof(arr);

	Bool rslt = Palindrome(arr, size/sizeof(int));

	if (rslt == true)
		printf("\nPALINDROME\n");
	else
		printf("\nNOT PALINDROME\n");

	return 0;
}